GloBuddies is a global friend discovery platform that breaks social and language barriers. It helps you connect with like-minded individuals from all across the world through a simple mobile app, while at the same time providing all the core features that an instant messaging app needs, and more.

There are 7 billion people in the world yet we interact with only a handful in our lifetime. We set out to build a service that could connect users across the world intelligently and seamlessly. This means a friend discovery system not limited by geographical and societal boundaries.

It is well known that certain personality types gel well together. A quiet personality often has very meaningful friendships with extroverted personalities. GloBuddies helps users discover more meaningful friendships through an intelligent friend discovery service.

The two core features of GloBuddies are its  its user profiling service and translation service. We now briefly explain their working.

The user profiling service estimates the personality traits of an individual based on their chat history. A new user’s personality is derived from their messaging history from their device. The IBM Watson User Modelling service is used to this end, giving a detailed description of the user’s traits.

Real time translation is the future of instant messaging. This service allows people speaking different languages to communicate with each other. 
In a chatroom environment, a group of people can talk to each other, each in their own native language.

The friend discovery service is guided by the user profiles on the backend. The personality traits work as features for suggesting similar users. This can be further improved by applying core AI algorithms with the objective of achieving higher chat times. For example, if person A talks more to person B he is likely to talk more to people similar to B. By taking into account the chat durations of two users, we can build a similarity index for the pair. 

GloBuddies is a complete package. Adding friends, updating your profile, viewing friends’ profile, creating group chats, sending multimedia messages, password lock are some essential features in the app.

It is not just an experimental prototype but a fully featured app deployed on IBM Bluemix.

Please have a look at the screenshots in the screenshots folder to get a feel of the UI of GloBuddies.