package com.cloverstudio.globuddies.couchdb;

public class SpikaException extends Exception {


	public SpikaException (String message) {
		super(message);
	}

}
